#!/usr/bin/env python3

"""
Plot pressures and pressure differences as read from the AEMET.
"""

import sys
import time
from datetime import datetime
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as fmt

import requests
import matplotlib
from matplotlib.dates import DateFormatter
import matplotlib.pyplot as plt
import numpy as np

# URLs for the location we are looking.
url_prefix = 'http://www.aemet.es/es/eltiempo/observacion/ultimosdatos_'
url_postfix = {
    'Albacete': '8178D_datos-horarios.csv?k=clm&l=8178D&datos=det',
    'Madrid':   '3195_datos-horarios.csv?k=mad&l=3195&datos=det',
    'Murcia':   '7178I_datos-horarios.csv?k=mur&l=7178I&datos=det',
    'Valencia': '8414A_datos-horarios.csv?k=val&l=8414A&datos=det',
    'Malaga':   '6156X_datos-horarios.csv?k=and&l=6156X&datos=det',
    'Javea':    '8050X_datos-horarios.csv?k=val&l=8050X&datos=det',
    'Benidorm': '8036Y_datos-horarios.csv?k=val&l=8036Y&datos=det'}

data_cols = {
    'temperature': 1,
    'wind': 2,
    'precipitation': 6,
    'pressure': 7,
    'humidity': 9}
# "Fecha y hora oficial","Temperatura (C)","Velocidad del viento (km/h)",
# "Direccion del viento","Racha (km/h)","Direccion de racha",
# "Precipitacion (mm)","Presion (hPa)","Tendencia (hPa)","Humedad (%)"


def main():
    args = get_args()

    data = read_file(args.file) if args.file else read_web(args.location)

    start = convert_date(args.start) if args.start else None
    end = convert_date(args.end) if args.end else None

    dates, data = aemet_csv_to_values(data, start, end, args.data_type)

    if data:
        plot(dates, data, args.location, args.data_type)
    else:
        print('Missing data.')


def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=fmt)

    add = parser.add_argument  # shortcut
    add('location', choices=url_postfix.keys(),
        help='location where we read data from')
    add('--data-type', choices=data_cols.keys(), default='pressure',
        help='type of meteorological data to plot')
    add('--file', help='read from file with saved historical data')
    add('--start', help='starting date (format: "d/m/Y H:M")')
    add('--end', help='ending date (format: "d/m/Y H:M")')

    return parser.parse_args()


def read_file(filename):
    print('Reading data from file:', filename)
    return open(filename, 'rb').read()


def read_web(location):
    print('Reading data from the aemet website.')
    url = url_prefix + url_postfix[location]
    return requests.get(url).content


def aemet_csv_to_values(contents, start=None, end=None, dtype='pressure'):
    "Return (dates, data) from the aemet's contents in csv format"
    col_data = data_cols[dtype]  # column with the meteorological data we want

    dates = set()
    pairs = []
    for line in contents.decode('utf8', errors='ignore').splitlines():
        cols = line.split('","')

        if len(cols) < 5 or line.startswith('"Fecha '):
            continue

        date_str = cols[0].strip('"')
        date = convert_date(date_str)

        if (start and date < start) or (end and date > end):
            continue  # skip dates outside of requested range

        try:
            data_str = cols[col_data].strip('"')
            data = float(data_str)
        except ValueError:
            print(f'Discarding wrong data on {date_str}: "{data_str}"')
            continue
        except IndexError:
            print(f'Discarding wrong line on {date_str}:', line)

        if date not in dates:
            pairs.append( (date, data) )
            dates.add(date)

    pairs.sort()
    return list(zip(*pairs)) if pairs else ([], [])


def convert_date(date):
    "Return datetime object that corresponds to date 'dd/mm/yyyy hh:mm'"
    t = time.mktime(time.strptime(date, '%d/%m/%Y %H:%M'))
    return datetime.fromtimestamp(t)


def plot(dates, data, location, dtype):
    fig, axs = plt.subplots(2, 1, figsize=(8, 4), dpi=120)
    fig.suptitle('Medidas de la AEMET en %s' % location, fontsize=10)
    set_sizes(axs)

    ylabel = {
        'temperature': 'temperatura\n(°C)',
        'wind': 'velocidad del viento\n(km/h)',
        'precipitation': 'precipitación\n(mm)',
        'pressure': 'presión\n(hPa)',
        'humidity': 'humedad\n(%)'}[dtype]

    plot_data(dates, data, axs[0], ylabel)
    plot_diff(dates, data, axs[1], ylabel)

    fig.autofmt_xdate()

    fname = '%s_%s.png' % (dtype, location.lower())

    print('Saving figure: %s' % fname)
    fig.savefig(fname)
    plt.show()


def set_sizes(axs):
    plt.rc('font', size=7)
    for ax in axs:
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                     ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(7)


def plot_data(dates, data, ax, ylabel):
    "Plot data in ax"
    ax.plot(dates, data, lw=2)
    if len(data) < 100:
        ax.plot(dates, data, 'bo', alpha=0.5)

    ax.set(ylabel=ylabel.capitalize())
    ax.grid()
    ax.set_xticks(dates, minor=True)


def plot_diff(dates, data, ax, ylabel):
    "Plot differences of data in ax"
    diff = np.diff(data)
    N = 3  # number of hours where a change is significant
    diff_smooth = smooth(diff, N)

    ax.plot(dates, np.insert(diff, 0, 0), alpha=0.2)
    ax.plot(dates[(N+1)//2:-(N-1)//2], diff_smooth)

    ax.axhline(color='black')
    ax.set(xlabel='Fecha', ylabel=('Cambios de ' + ylabel))
    ax.xaxis.set_major_formatter(DateFormatter('%d %b %k:%M'))
    ax.grid()
    ax.set_xticks(dates, minor=True)


def smooth(x, n):
    "Return a running mean of x (with an average of n points for each x[i])"
    # See https://stackoverflow.com/questions/13728392
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[n:] - cumsum[:-n]) / n



if __name__ == '__main__':
    main()
