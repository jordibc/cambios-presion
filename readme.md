# Cambios de presión

Toma los datos de presión de la [Agencia Estatal de
Meteorología](http://www.aemet.es) y hace un plot con las últimas
presiones y las diferencias de presión que ha habido.


## Ejemplos

```sh
$ ./cambios_presion.py Albacete
```

produce algo como

![Plot](examples/presiones.png)

Para leer de un fichero y seleccionar solo los datos posteriores a
cierta fecha:

```sh
./cambios_presion.py Madrid --file presion_madrid.csv --start '01/09/2023 00:00'
```


## Toma automática de datos

Como no parece haber ningún histórico de datos de presión, y en cada
consulta solo aparecen los datos correspondientes a las últimas 24
horas, hay que recoger automáticamente esa información.

Por ejemplo con las siguientes líneas en el crontab:

```crontab
0 2,14 * * * wget 'http://www.aemet.es/es/eltiempo/observacion/ultimosdatos_8178D_datos-horarios.csv?k=clm&l=8178D&datos=det' -O - -nv >> presion_albacete.csv
1 2,14 * * * wget 'http://www.aemet.es/es/eltiempo/observacion/ultimosdatos_7178I_datos-horarios.csv?k=mur&l=7178I&datos=det' -O - -nv >> presion_murcia.csv
2 2,14 * * * wget 'http://www.aemet.es/es/eltiempo/observacion/ultimosdatos_8414A_datos-horarios.csv?k=val&l=8414A&datos=det' -O - -nv >> presion_valencia.csv
3 2,14 * * * wget 'http://www.aemet.es/es/eltiempo/observacion/ultimosdatos_6156X_datos-horarios.csv?k=and&l=6156X&datos=det' -O - -nv >> presion_malaga.csv
4 2,14 * * * wget 'http://www.aemet.es/es/eltiempo/observacion/ultimosdatos_3195_datos-horarios.csv?k=mad&l=3195&datos=det' -O - -nv >> presion_madrid.csv
```

### ¿Mejoras futuras?

Puede que podamos sacar todos esos datos y más de
[Open-Meteo](https://open-meteo.com/). ¡Estaría bien explorarlo!
